<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Services\CategoriesServices;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    public function __construct(CategoriesServices $categoriesServices)
    {
        $categoriesServices->updateSession();
    }
    
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request,CategoryRepository $repoCat, EntityManagerInterface $em): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        $session = $request->getSession();
        // Est ce que le formulaire est soumis?
        if($form->isSubmitted() && $form->isValid()){
            $contact->setCreatedAt(new \DateTimeImmutable());
            $em->persist($contact);
            $em->flush();
            $contact = new Contact();
            $form =  $this->createForm(ContactType::class, $contact); 
 
            
            $session->getFlashBag()->add("message", "Message sent successfully");
            $session->set('status', "success");
            
        }else if($form->isSubmitted() && ! $form->isValid()){
            $session->getFlashBag()->add("message", "Please correct the errors");
            $session->set('status', "danger");

        }

        return $this->render('contact/contact.html.twig', [
            'contact' => $form->createView()
        ]);
    }
}
