<?php

namespace App\Controller;

use App\Services\CategoriesServices;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContactController extends AbstractController
{
    public function __construct(CategoriesServices $categoriesServices)
    {
        $categoriesServices->updateSession();
    }
    
    #[Route('/contact', name: 'app_contact')]
    public function index(): Response
    {
        $form = $this->createFormBuilder()
                     ->add("Email", EmailType::class, [
                        "label" =>"Votre Email",
                        'attr' => [
                            "placeholder" => " Votre email svp",
                            "class" => "form-group"
                        ],
                        "row_attr" =>[
                            "class" => "form-group"
                        ]
                     ])
                     ->add("Subject", TextType::class,  [
                        "label" =>"Votre Objet",
                        'attr' => [
                            "placeholder" => " Votre objet svp",
                            "class" => "form-group"
                        ],
                        "row_attr" =>[
                            "class" => "form-group"
                            ]
                     ])
                     ->add("Message", TextareaType::class,  [
                        "label" =>"Votre Message",
                        'attr' => [
                            "placeholder" => " Votre Message svp",
                            "class" => "form-group"
                        ],
                        "row_attr" =>[
                            "class" => "form-group"
                            ]
                     ])
                     ->add("Envoyer_message", SubmitType::class, [
                        "attr" => [
                            "class" => "btn"
                        ]
                     ])
                     ->getForm()
        ;
        return $this->render('contact/index.html.twig', [
            'contact' => $form->createView()
        ]);
    }
}
