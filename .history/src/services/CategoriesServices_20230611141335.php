<?php 
namespace App\Services;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class CategoriesServices {
    private $repoCategory;
    private $requestStack;
    public function __construct(RequestStack $requestStack, CategoryRepository $repoCategory){
        $this->repoCategory = $repoCategory;
        $this->requestStack = $requestStack;

    }

    public function updateSession(){
        $session = ^$this->requestStack->getSession();
        $categories = $this->repoCategory->findAll();
        $session->set("categories", $categories);
    }
    
}